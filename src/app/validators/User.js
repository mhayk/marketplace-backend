const Joi = require('joi')

module.exports = {
  body: {
    name: Joi.string().required(),
    email: Joi.string()
      .email()
      .required(),
    price: Joi.number()
      .required()
      .min(6)
  }
}
