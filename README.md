# marketplace-backend

Marketplace App

# Docker

\$ docker run --name mongonode -p 27017:27017 -d -t mongo
\$ docker ps

# Redis

\$ docker run --name noderedis -p 6379:6379 -d -t redis:alpine
